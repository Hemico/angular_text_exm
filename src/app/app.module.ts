import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';


import { RouterModule } from '@angular/router';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersService } from "./users/users.service";
import { HttpModule } from '@angular/http';

import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AddFormComponent } from "./users/add-form/add-form.component";
import { UpdateFormComponent } from "./users/update-form/update-form.component";
import { UserComponent } from './users/user/user.component';
//Fire Base
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';
import { UsersfireComponent } from "./usersfire/usersfire.component";



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    AddFormComponent,
    UserComponent,
    UpdateFormComponent,
    UsersfireComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path: '', component: UsersComponent}, // דף ראשי
      {path: 'users', component: UsersComponent},
      {path: 'user/:id', component: UserComponent},
      {path: 'update-form/:id', component: UpdateFormComponent},
      {path: 'products', component: ProductsComponent},
      {path: 'usersfire', component: UsersfireComponent},
      {path: '**', component: NotFoundComponent}//להשאיר בכל מקרה
    ])

  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
