import { UsersService } from './../users.service';
import { Component, OnInit , Output , EventEmitter } from '@angular/core';
import { FormGroup , FormControl ,FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css']
})
export class AddFormComponent implements OnInit {
  @Output() addUser:EventEmitter<any> = new EventEmitter<any>();
  @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>();
  service:UsersService;
  addform = new FormGroup({
      name:new FormControl(),
      phone:new FormControl()
  });

   //שליחת העדכון דרך הסרוויס 
  //username;
  //email;
  sendData() {
    //השורה שולחת את התוכן לאופטימיסטיק שנמצא באב
    this.addUser.emit(this.addform.value.name); //עדכון לאב בהתאם לשדות שהגדרנו למעלה
    
    console.log(this.addform.value);
    this.service.postUsers(this.addform.value).subscribe(
      response => {
        console.log(response.json());
        this.addUserPs.emit();
      }
    );
  }

  
    constructor(service: UsersService, private formBuilder:FormBuilder) { 
    this.service = service;
  }

  ngOnInit() {
  }

}
