// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: 'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyA4orxF6llHjfCOvAjZNZ8KLmLqh6DFuHA",
    authDomain: "testbefor.firebaseapp.com",
    databaseURL: "https://testbefor.firebaseio.com",
    projectId: "testbefor",
    storageBucket: "testbefor.appspot.com",
    messagingSenderId: "11211394392"
  }
};